## Introduction
This project works as an Openstack client, designed to automatically deploy "ready to work" instances of consultation tools (such as Democracyos, Consul and Madison) on an OPENSTACK Cloud. 
Each instance is independent and has its own IP. 

Here are the steps of a deployment :

1. Creation of an instance of the chosen image (Consul, DemocracyOS or Madison) on OpenStack ;
2. Perform postinstall operations : (DNS configuration, add admin informations, TLS certificate, Email configuration) 
3. Email the user about the status of deployment (failed or OK)



## System Requirements

Prerequisites 

```
Linux Distribution (Debian 8 was used by Etalab)
DNS server (BIND) 

```

- To allow your user to connect in ssh on your new instance without host verification, create a file  /home/$user-for-instance-creation/.ssh/config and write into him

```
Host *
   StrictHostKeyChecking no
   UserKnownHostsFile=/dev/null
```

- To allow your users to execute command as root without password, edit your /etc/sudoers file as below.

```
%sudo   ALL=(ALL:ALL) ALL
user-for-instance-creation ALL=(ALL) NOPASSWD: ALL

```

## Openstack Installation and setting configuration file to automatic deployment 

- Install Openstack API client package
```
apt-get install python-openstackclient
```
- If you want handle Openstack API in your terminal with command-line, you must add this line in your .bashrc file.
```
source $Path/To/openrc.sh/file
```
- To add your Domain Zone in automatic way, just add this line again in your .bashrc file
```
export DOMAINNAME="$YOUR_DOMAIN_NAME"
```
- Script : Get user info, call deploy.sh to create instance with user info, declare subdomain in your BIND DNS and exec postinstall script which matched with image dpeloyed (deploy.php)

Exemple with PHP :
```
<?php

//retrieve your variables with your favorite method !

// Launch instance creation script
$instance_ip=shell_exec('/path/to/deploy.sh "' .$name. '" "' . $email . '" "' .$tool. '" "' .$caller_name. '" "' .$subdomain. '"');
    

// Add subdomain to bind DNS server and redirect them to instance ip
shell_exec('/usr/bin/sudo /path/to/postinstall/add_A_bind_record.sh "' .$subdomain. '" "' .$instance_ip. '"');

// Launch post installation script for the chosen tool to made an instance "ready to use"

$output=shell_exec('/usr/bin/sudo -u $sudo_user /path/to/postinstall/' .$tool. '/' .$tool. '.sh "' .$name. '" "' . $email . '" "' .$caller_name. '" "' .$subdomain. '" "' .$instance_ip. '"');

?>
```





- Script : Create an instance with user info retrieve in deploy.php and get his IP adress (deploy.sh)

```
#!/bin/bash

#Connect to openstack cloud with credential file
source $Path/To/openrc.sh/file

#Get Image ID with his name
imageid=$(openstack image list | grep "$image_name" | cut -d "|" -f2)

#Create instance and get his info
instance=$(nova boot --flavor vps-ssd-1 --image $imageid --security-groups default $name_instance)

#Get instance IP 
instance_id=$(echo $instance | cut -d "|" -f48)
instance_state=$(nova list | grep "$instance_id")
instance_ip=$(echo $instance_state |  cut -d '|' -f7 | cut -d '=' -f2)

sleep 45
if ping -q -c 1 "$instance_ip" > /dev/null
then
    echo -e "view instance : https://"$subdomain".domain.tld"  | mail -s "Your instance was deployed" $user_mail
    echo -e " Admin, an instance deploy with "$image_name" image, Name of instance :"$name_instance" " | mail -s "A new instance was created " $admin_mail
    echo $instance_ip
else
    echo -e "Error with "$name_instance" "  | mail -s "Erreur "$name_instance" " $user_mail
    echo -e "Admin, Error with "$name_instance" instance "  | mail -s "Error with "$name_instance" " $admin_mail
fi
```
- Script : Add subdomain record in BIND DNS and redirect him to instance IP adress (add_A_bind_record.sh)

```
#!/bin/bash

echo $1 IN A $2 >> /etc/bind/db.$DOMAINNAME
rndc reload
systemctl restart bind9
```


