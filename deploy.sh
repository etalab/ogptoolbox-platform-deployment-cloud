#!/bin/bash

#Connect to openstack cloud with credential file
source /opt/openrc.sh

nameconsultation=$2"_"$5

#Get image id with the name of image get in deploy_vm.php
imageid=$(openstack image list | grep "$3" | cut -d "|" -f2)

#Create instance
instance=$(nova boot --flavor vps-ssd-1 --image $imageid --security-groups default --poll "$nameconsultation")
instance_id=$(echo $instance | cut -d "|" -f48)
instance_state=$(nova list | grep "$instance_id")
instance_ip=$(echo $instance_state |  cut -d '|' -f7 | cut -d '=' -f2)

sleep 45
if ping -q -c 1 "$instance_ip" > /dev/null
then
    echo -e "Bonjour Monsieur ou Madame "$4",\nNous vous confirmons que votre consultation '"$1"' a été déployée gratuitement avec '"$3"',\nVoir votre consultation : https://"$5"."$6" \nVous recevrez bientôt un mail vous expliquant plus en détail la prise en main de l'outil de consultation que vous avez choisi."  | mail -s "Votre consultation est en ligne !" mail -r ne-pas-repondre@consultation.etalab.gouv.fr $2
    echo -e "Hello Administrator,\nThere is the specs of the new instance created with the ogptoolbox : \n-Username :"$4"\n-Image :"$3"\n-Name of instance :"$1"\n-IP adress :"$instance_ip | mail -s "A new instance was created by "$4"." tweiss@octo.com
    echo $instance_ip
else
    echo -e "Bonjour Monsieur ou Madame "$4",\nNous sommes désolés un problème est survenu lors du déploiement de votre consultation '"$1"'. Veuillez contacter Etalab ou recommencer ultérieurement."  | mail -s "Erreur : votre consultation '"$1"' n'a pas fonctionné" mail -r ne-pas-repondre@consultation.etalab.gouv.fr $2 
    echo -e "Bonjour Admin,\nN la consultation "$1" n'a pas fonctionné"  | mail -s "Erreur : votre consultation "$1" n'a pas fonctionné" tweiss@octo.com
fi
