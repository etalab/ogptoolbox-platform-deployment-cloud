#!/bin/bash


ip=$(echo "$6" | sed -e "s/ *$//")
affichip=$ip
test=$(curl --request POST \
  --url https://api.sendgrid.com/v3/access_settings/whitelist \
  --header 'Authorization:Basic Y29uc3VsdGF0aW9uZXRhbGFiOmNvbnN1bGV0YWxhYjE3' \
  --header 'Content-Type: application/json' \
  --data '{
           "ips":[
                {
                "ip":"'$ip'"
                }]
        }')


domain="$4.$5"
email="$2"
sudo -u ogptoolbox ssh -tq ogptoolbox@$domain "sudo sed -i 's/EMAILUSER/$email/' /home/democracy/democracyos-plateforme-consultation-etat/config/production.json
sudo sed -i 's/SUBDOMAIN/$4/' /home/democracy/democracyos-plateforme-consultation-etat/config/production.json
sudo sed -i 's/CRED_USER/democracyosconsult/' /home/democracy/democracyos-plateforme-consultation-etat/config/production.json
sudo sed -i 's/CRED_PASS/democonsult2017/' /home/democracy/democracyos-plateforme-consultation-etat/config/production.json
sudo sed -i 's/DOMAIN/$domain/' /home/democracy/democracyos-plateforme-consultation-etat/config/production.json
sudo service nginx stop
sudo mv /home/democracy/default /etc/nginx/sites-available/.
sudo sed -i 's/DOMAIN/$domain/' /etc/nginx/sites-available/default
sudo service nginx restart"
sudo -u ogptoolbox ssh -tq democracy@$domain "cd democracyos-plateforme-consultation-etat/ && bash -lc 'pm2 start npm -- start'"
sudo echo -e "Bonjour Madame ou Monsieur "$3",\nMerci d'avoir déployé votre consultation avec l'outil democracyOS,\nVoici la documentation de ce dernier pour vous permettre de prendre en main l'outil : http://democracyos.eu/docs-fr/tuto/2016/09/22/inscription.html ,\nEn tant qu'administrateur de votre consultation vous avez différents privilèges sur celle-ci,\nPour profiter de ces privilèges,créez vous un compte sur votre consultation https://"$4"."$5" avec la même adresse email que vous aviez utilisé pour déployé cette consultation, vous recevrez un email pour confirmer votre compte\nUne fois la vérification de l'adresse email faite vous serez reconnu en tant qu'administrateur de la consultation !" | mail -s "Prendre en main votre nouvelle consultation !" mail -r democracyOS@$4.$5 $2
#sudo letsencrypt certonly --standalone --domain $domain --email steven.snoussi@data.gouv.fr --agree-tos -n 

