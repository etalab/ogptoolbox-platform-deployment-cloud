#!/bin/bash

ip=$(echo "$6" | sed -e "s/ *$//")
affichip=$ip
test=$(curl --request POST \
  --url https://api.sendgrid.com/v3/access_settings/whitelist \
  --header 'Authorization:Basic Y29uc3VsdGF0aW9uZXRhbGFiOmNvbnN1bGV0YWxhYjE3' \
  --header 'Content-Type: application/json' \
  --data '{
           "ips":[
                {
                "ip":"'$ip'"
                }]
        }')

domain="$4.$5"
email="$2"
name="$3"
fname=$(echo "$3" | cut -d " " -f1)
lname=$(echo "$3" | cut -d " " -f2)
sudo -u ogptoolbox ssh -tq ogptoolbox@$domain "sudo systemctl stop apache2
sudo atp remove apache2
sudo systemctl stop nginx
sudo sed -i 's/DOMAIN/$domain/' /home/madison/ogptoolbox-platform-partners-madison/server/.env
sudo sed -i 's/CRED_USER/madisonconsult/' /home/madison/ogptoolbox-platform-partners-madison/server/.env
sudo sed -i 's/CRED_PASS/madconsult2017/' /home/madison/ogptoolbox-platform-partners-madison/server/.env
sudo sed -i 's/EMAIL_USER/$email/' /home/madison/ogptoolbox-platform-partners-madison/server/.env
sudo sed -i 's/admin@example.com/$2/' /home/madison/ogptoolbox-platform-partners-madison/server/config/madison.php
sudo sed -i 's/First/$fname/' /home/madison/ogptoolbox-platform-partners-madison/server/config/madison.php
sudo sed -i 's/Last/$lname/' /home/madison/ogptoolbox-platform-partners-madison/server/config/madison.php
sudo letsencrypt certonly --standalone --domain $domain --email steven.snoussi@data.gouv.fr --agree-tos -n 
sudo sed -i 's/DOMAIN/$domain/' /etc/nginx/sites-available/default
sudo systemctl unmask nginx
sudo apt install nginx -y
cd /home/madison/ogptoolbox-platform-partners-madison/ && bash -lc 'sudo make'
cd /home/madison/ogptoolbox-platform-partners-madison/ && bash -lc 'sudo make db-reset'
cd /home/madison/ogptoolbox-platform-partners-madison/ && bash -lc 'sudo make queue-listen'
sudo systemctl start nginx"
