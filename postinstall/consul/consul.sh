#!/bin/bash

ip=$(echo "$6" | sed -e "s/ *$//")
affichip=$ip
test=$(curl --request POST \
  --url https://api.sendgrid.com/v3/access_settings/whitelist \
  --header 'Authorization:Basic Y29uc3VsdGF0aW9uZXRhbGFiOmNvbnN1bGV0YWxhYjE3' \
  --header 'Content-Type: application/json' \
  --data '{
           "ips":[
                {
                "ip":"'$ip'"
                }]
        }')

domain="$4.$5"
sudo -u ogptoolbox ssh -tq ogptoolbox@$domain "sudo systemctl stop nginx
sudo letsencrypt certonly --standalone --domain $domain --email steven.snoussi@data.gouv.fr --agree-tos -n 
sudo rm -f /etc/nginx/sites-available/default.old
sudo mv /home/consul/default /etc/nginx/sites-available/.
sudo sed -i 's/DOMAIN/$domain/' /etc/nginx/sites-available/default
sudo sed -i 's/DOMAIN/$domain/' /home/consul/ogptoolbox-toolbox-partners-consul/config/environments/development.rb
sudo sed -i 's/CRED_USER/consulconsult/' /home/consul/ogptoolbox-toolbox-partners-consul/config/environments/development.rb
sudo sed -i 's/CRED_PASS/consult2017/' /home/consul/ogptoolbox-toolbox-partners-consul/config/environments/development.rb
sudo systemctl restart nginx"
sudo -u ogptoolbox ssh -tq ogptoolbox@$domain "cd /home/consul/ogptoolbox-toolbox-partners-consul/ && sudo -u consul bash -lc 'rails server -d'"

