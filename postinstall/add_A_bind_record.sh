#!/bin/bash

sudo echo $1 IN A $3 >> /etc/bind/db.$2
sudo rndc reload
sudo systemctl restart bind9
