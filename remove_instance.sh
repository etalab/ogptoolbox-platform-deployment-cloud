#!/bin/bash

#Connect to openstack cloud with credential file
source /opt/openrc.sh

# When you execute this script set a first argument as the subdomain and for the second argument the domain name
domain="$1.$2"
echo "récupération de l'IP"
match_ip=$(nslookup $domain | cut -d ":" -f2 | tail -n 2)
instance_ip=$(echo "$match_ip" | sed -e "s/ *$//")
echo "IP : $instance_ip"
echo "récupération de l'ID"
instance_id=$(nova list | grep "$1" | cut -d "|" -f2)
echo "ID : $instance_id"
if ping -q -c 1 $instance_ip > /dev/null
then

echo "accès réseau à l'instance confirmé !"
echo "connexion à l'instance..."
sudo -u ogptoolbox ssh -tq ogptoolbox@$domain'sudo systemctl stop nginx'

echo "révocation du certificat ssl..."
sudo -u ogptoolbox ssh -tq ogptoolbox@$domain'sudo letsencrypt revoke -d $domain --cert-path /etc/letsencrypt/live/$domain/cert.pem'

echo "suppression de la redirection de nom..."
sudo sed -i -e '/'$1'/d' /etc/bind/db.$2
sudo rndc reload
sudo systemctl restart bind9

echo "suppresion de l'instance dans la base mongo"
sudo mongo --port 27017 -u "user" -p "pass" --authenticationDatabase "db_deploy"<<EOF
use db_deploy
db.consultations.remove( { slug : "$1" } )
EOF

echo "suppression de l'instance..."
openstack server delete $instance_id

else

echo "instance non active sur le réseau !"

fi
