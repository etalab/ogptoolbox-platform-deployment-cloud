#!/bin/bash

domain="$1.$2"
true_instance_ip="$3"
echo "récupération de l'IP"
match_ip=$(nslookup $domain | cut -d ":" -f2 | tail -n 2)
instance_ip=$(echo "$match_ip" | sed -e "s/ *$//")

echo "test de la redirection DNS..."
if [ $instance_ip != $true_instance_ip ]
then
	echo "erreur de redirection DNS !"
	echo "le nom de domaine pointe vers "$instance_ip" au lieu de "$true_instance_ip " !" 
	exit 0
else
	echo "le nom de domain est bien redirigé vers "$true_instance_ip" !"
fi

echo "test de connectivité via l'IP...."
if ping -q -c 1 $instance_ip > /dev/null
then
	echo "connexion réussi !"
else
	echo "echec de connexion, l'adresse IP spécifié n'est pas disponible sur le réseau !"
	exit 0
fi

echo "test de connectivité via le domaine...."
if ping -q -c 1 $domain > /dev/null
then
	echo "connexion réussi !"
else
	echo "echec de connexion, le domaine spécifié n'est pas disponible sur le réseau !"
	exit 0
fi

echo "test de la validité du certificat...."
check_cert=$(cd /home/ogptoolbox/ && bash -lc './ssl-cert-check -s '$domain' -p 443 | tail -n 1 | sed "s/\ \ */|/g" | cut -d "|" -f2')
if [ $check_cert != "Valid" ]
then
	echo "certificat invalide !"
	exit 0
else
	echo "certificat valide !"
fi

echo "tout les test sont OK ! notification de l'utilisateur du bon déploiement de la consultation..."

